var response = context.getVariable("xmlResponse.SABbody");
var responseCode = context.getVariable("xmlResponse.responseCode");
var asOfDateTime = context.getVariable("xmlResponse.asOfDateTime");

if (typeof response != 'undefined') {
    var responseBody = JSON.parse(response);
    //var errorType = context.getVariable("responseBody.result.ChannelErrors.error.ErrorMessage.TEXT");
    var correlationId = context.getVariable("request.header.X-Correlation-ID");

    //var checkDigitId = responseBody.accountNumber.toString();
    /*var customerNumber = responseBody.CustomerNumber.toString();
    var currentBalance = responseBody.CurrentBalance.toString();
    var actualCurrentBalance = responseBody.ActualCurrentBalance.toString();
    var currentOverdueBalance = responseBody.CurrentOverdueBalance;
    var asOfDateTime = context.getVariable("xmlResponse.asOfDateTime");
    
    //CRS does not speak milliseconds, so we add default milliseconds of .000 to follow our standard 
    var asOfDateTimePlusMS = asOfDateTime.concat(".000");*/
    
    var responseObject = {};
    var billingAccounts = [];
    var finalObject = {};

    if (responseCode == 'Success') {
        
    var customerNumber = responseBody.CustomerNumber.toString();
    var currentBalance = responseBody.CurrentBalance.toString();
    var actualCurrentBalance = responseBody.ActualCurrentBalance.toString();
    var currentOverdueBalance = responseBody.CurrentOverdueBalance;
    var asOfDateTime = context.getVariable("xmlResponse.asOfDateTime");
    
    //CRS does not speak milliseconds, so we add default milliseconds of .000 to follow our standard 
    var asOfDateTimePlusMS = asOfDateTime.concat(".000");
        
        context.setVariable('response.status.code', 200);

        // map correlationId
        if (correlationId != "NULL") {
            responseObject.correlationId = correlationId;
        } else {
            responseObject.correlationId = "";
        }

        // map billingId
        if (customerNumber != "NULL") {
            finalObject.billingId = customerNumber;
        } else {
            finalObject.billingId = "";
        }

        //map balance
        //var balancesArray = [];
        var balanceObject = {};

        // map currentBalance
        if (currentBalance != "NULL") {
            if (currentBalance === '0') {
               balanceObject.current = Number(0).toFixed(2);
            } else {
               balanceObject.current =  currentBalance;
            }
        } else {
           balanceObject.current =  '';
        }
        
        // map actualCurrentBalance
        if (actualCurrentBalance != "NULL") {
            if (actualCurrentBalance === "0") {
               balanceObject.actualCurrent = Number(0).toFixed(2);
            } else {
               balanceObject.actualCurrent = actualCurrentBalance;
            }
        } else {
               balanceObject.actualCurrent = "";
        }
        
        //map currentOverdueBalance
        if (currentOverdueBalance != "NULL") {
           if (currentOverdueBalance === 0) {
            print("we are at zero balance");
              balanceObject.pastDue = Number(0).toFixed(2);
           } else {
              balanceObject.pastDue = currentOverdueBalance;
           }
        } else {
              balanceObject.pastDue = "";
        }
        
        
        //map asOfDateTime
        if (asOfDateTime != "NULL") {
            balanceObject.asOfDateTime = asOfDateTimePlusMS;
        } else {
            balanceObject.asOfDateTime = "";
        }

        //balancesArray.push(balanceObject);
        
        /*if( balancesArray.length > 0) {
            finalObject.accountBalances = balancesArray;
        }*/

        //finalObject.accountBalances = balancesArray;
        
        finalObject.accountBalances = balanceObject

        billingAccounts.push(finalObject);
        responseObject.billingAccounts = billingAccounts;

    } else {
        context.setVariable('response.status.code', 404);
        
        var errorDetail = context.getVariable("xmlResponse.errorDetail");
        var errorAsOfDateTime = context.getVariable("xmlResponse.errorAsOfDateTime");
        //var responseMessage = responseCode;
        context.setVariable("isValidResponse", false);
        context.setVariable("responseMessage", errorDetail);
        context.setVariable("errorTS", errorAsOfDateTime);

    }


    context.setVariable("finalResponse", JSON.stringify(responseObject));


}